# -*- coding: utf-8 -*-

import sys
import pygame

from pygame.locals import Rect

class Cell(object):

    def __init__(self, state, location, size):
        self.state = state or False
        self.futurestate = False
        self.location = location or sys.exit('Parameter location for class Cell was invalid.')
        self.size = size or sys.exit('Parameter size for class Cell was invalid.')

    def getColor(self):
        color = (0, 0, 0)
        
        if self.state:
            color = (255, 255, 255)

        return color

    def getRect(self):
        return Rect(self.location, self.size)

    def getLocation(self):
        return self.location

    def getState(self):
        return self.state

    def setState(self, state):
        self.state = state

    def getFutureState(self):
        return self.futurestate

    def setFutureState(self, state):
        self.futurestate = state

    def checkState(self, cells):
        alive = 0
        for cell in cells:
            if cell is not None and cell.getState():
                alive += 1

        # 1. Under-population
        if self.getState and alive < 2:
            self.setFutureState(False)
        # 2. Survive
        elif self.getState and alive in (2,3):
            self.setFutureState(True)
        # 3. Overcrowding
        elif self.getState and alive > 3:
            self.setFutureState(False)
        # 4. Reproduction
        elif not self.getState and alive == 3:
            self.setFutureState(True)
