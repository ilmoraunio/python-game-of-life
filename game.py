# -*- coding: utf-8 -*-

import sys
import pygame
import random

from cell import Cell
from grid import Grid
from pygame.locals import *

pygame.init()

XDIM = 640
YDIM = 480
CELLS = dict(X=100, Y=100)

def randomizeState(x=None, y=None):
    # magic numbers should be put as parameters
    isalive = random.choice(range(0,10)) == 0 and x < 10 and y < 10
    return isalive

# two-dimensional grid + seed
Grid_ = Grid(
            [[Cell(randomizeState(x=i, y=n), 
                  ((XDIM / CELLS['Y']) * i, (YDIM / CELLS['Y']) * n), 
                   (XDIM / CELLS['X'], YDIM / CELLS['X'])) 
                    for i in range(0, CELLS['X'])]
                        for n in range(0, CELLS['Y'])])

# create the screen
window = pygame.display.set_mode((XDIM, YDIM))

# draw grid
for row in Grid_:
    for cell in row:
        pygame.draw.rect(window, cell.getColor(), cell.getRect())

# refresh screen
pygame.display.flip()

# game loop
while True:

    # check for state
    for y in range(0, CELLS['Y']):
        for x in range(0, CELLS['X']):
            cell = Grid_[x][y]
            neighbours = Grid_.getNeighbours(dict(x=x, y=y))
            cell.checkState(neighbours)

    # propagate state change (set future states into present states)
    Grid_.setNextTick()

    for row in Grid_:
        for cell in row:
            pygame.draw.rect(window, cell.getColor(), cell.getRect())

    pygame.display.flip()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit(0)
        else:
            print(event)
