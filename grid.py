# -*- coding: utf-8 -*-
from __future__ import division

from cell import Cell
from itertools import islice

import sys
import random

class Grid(object):

    def __init__(self, seed):
        self.grid = seed or sys.exit('Parameter seed for class Grid was invalid.')

    def __getitem__(self,index):
        if index < 0:
            return None
        try:
            return next(islice(self.grid, index, index + 1))
        except IndexError:
            return None

    def __setitem__(self,index,val):
        self.grid[index] = val

    def __len__(self):
        return len(self.grid)

    def getGrid(self):
        return self.grid

    # pos = {'x', 'y'}
    def getNeighbours(self, pos):
        indexes = (
            dict(x=pos['x'] - 1, y=pos['y'] - 1),
            dict(x=pos['x'], y=pos['y'] - 1),
            dict(x=pos['x'] + 1, y=pos['y'] - 1),
            dict(x=pos['x'] - 1, y=pos['y']),
            dict(x=pos['x'], y=pos['y']),
            dict(x=pos['x'] + 1, y=pos['y']),
            dict(x=pos['x'] - 1, y=pos['y'] + 1),
            dict(x=pos['x'], y=pos['y'] + 1),
            dict(x=pos['x'] + 1, y=pos['y'] + 1)
        )

        retval = []
        for node in indexes:
            if node['x'] >= 0 and \
               node['y'] >= 0 and \
               node['x'] < len(self.grid) and \
               node['y'] < len(self.grid[node['x']]):
                retval.append(self.grid[node['x']][node['y']])
            else: 
                retval.append(None)

        return retval

    def setNextTick(self):
        for row in self.grid:
            for cell in row:
                cell.setState(cell.getFutureState())